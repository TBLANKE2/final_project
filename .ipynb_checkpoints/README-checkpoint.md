# Final Project for AOS5723


## Description

This notebook attempts to recreate plots that were made using GEMPAK, an application that was made to display meteorological data created in the 1980's. While
GEMPAK is still useful in some cases, the technology for it is outdated and it is not the easiest software to work with, so I decided to see if I could make plots
that are as good or better using Python3. The main libraries I used were MetPy, Xarray, Matplotlib, and Cartopy. These will allow me to plot different
meteorological variables over maps of the Earth in the most efficeint way. The example data I used in this notebook is from 1997 so the resolution and the variables
available are a little limited but they work fine for synoptic level analysis. The path to these files is listed below.
The most important library used in this notebook is MetPy. MetPy is a fairly new library that is attempting to replace GEMPAK, or at least bring the functionality
of GEMPAK to the modern age using Python. It is still being worked on to impliment the same functions that GEMPAK has so there are still some that the user will
have to figure out themselves, one example being stability. When finished, this would be a huge step forward as python has a lot more options to customize your
plots than GEMPAK. Python also is easier to change since if you are in a jupiter notebook, you can quick change a couple variables and then run it as with GEMPAK it
can be a bit more clunky as you either have to change the .csh script or manually type out each variable name and what you want it to be changed to. 

The first four panel plot is of negative omega and geopotential height at 700mb over the course of four days starting on April 4th, 1997. This is important to look
at because rising air evacuates mass out of the column which will intensify or move a surface low. Looking at how omega changes is a good start to why cyclones
intensify or die off. 
The second four panel plot is valid for one time and looks at multiple variables. In the GEMAPK panel 1, it has 1000mb temperature and geopotential height. The
python panel 1 excludes temperature since I was unable to get the file for it. These two variables show any surface lows as well as any clearly defined fronts that
may arise. These features are very important to keep track of since they are normally assosiated with severe weather. The temperture potentially shows the type of
precipitation that will be associated with the storm as well as being use to make a temperature forecast. Panel 2 shows 300mb geopotential height, divergence of the 
ageostrophic wind, and the actual wind. These are also important to diagnose the cause of an intensifying or weakening of a low thorughout its life. The 300mb jet 
is associated with vertical motions as well so depending on where it is with respect to the low wil determine its effect if any. Upper level trouphs and ridges act 
similarly to jets and can affect different regions than the jets or intensify the jets' impact on vertical motion. Divergence will show where air is rising and thus 
if the surface low will move and intensify or weaken. GEMPAK panel 3 plots relative humidity and stability. Python panel 3 excludes stability as it is not a 
function usable in MetPy and I did not have access to potential temperature to calculate it. Relative humidity is one part that gives us a good idea as to how 
intense the storm will be based on the amount of water vapor present. The second half to that is statbility as this will show how easy it is to lift that water 
vapor up in order for it to condense into precipitable water. Then the final plot shows 500mb geopotential height and relative vorticity. The heights allows us to 
orient ourselves and how the upperair winds are behaving. Relative vorticity shows us where there is rotation which is indicative of vertical motion. Putting all of 
these pannels together gives us a good start to diagnosing how the suface low will act throughout its lifecycle. 
Both of these plots are very useful since they can be used to research old storms that may have had ambiguous origins as well as to predict how new storms will form 
and act through the use of forecasting models. 

I do realize that MetPy has a built in plotting function that will make these plots easier to display and change and this is something I will look into in the future but this notebook is about using matplotlib and cartopy to create plots as that is what I orginally sought out to do. 

## Getting Started
To get this notebook to your own computer, you can use this command in a non github directory - 
                git clone https://git.doit.wisc.edu/TBLANKE2/final_project.git
The environment I used was the AOS573 environment and is attached so then you will have everything you need to run this notebook and more if you so choose. 

## Files
The path to grb files for the 1997 case: /home3/cases/573cases/grib/    
Each file contains a single variable for one day in the form of a grb file. 

## Authors and acknowledgment
Author: Tanner Blanke
Acknowledgments: Hannah Zanowski, Cameron Bertossa for help and support thorughout the class 

Documentations for 
xarray:  https://docs.xarray.dev/en/stable/
numpy: https://numpy.org/doc/
matplotlib: https://matplotlib.org/stable/index.html
cartopy: https://scitools.org.uk/cartopy/docs/latest/
metpy: https://unidata.github.io/MetPy/latest/userguide/index.html